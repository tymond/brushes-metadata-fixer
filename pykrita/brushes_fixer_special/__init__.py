from .brushes_fixer_special import BrushesFixerSpecial

# And add the extension to Krita's list of extensions:
app = Krita.instance()
# Instantiate your class:
extension = BrushesFixerSpecial(parent = app)
app.addExtension(extension)
