# BBD's Krita Script Starter Feb 2018

# Brushes Fixer
# Copyright (C) 2022 Agata Cacko cacko.azh@gmail.com

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.



from krita import Extension
from PyQt5.QtCore import (QFile, QFileInfo, QRegularExpression)
from PyQt5.QtGui import QImage
from PyQt5.QtWidgets import (QDialog, QMessageBox, QTextEdit, QPushButton, QVBoxLayout, QFileDialog, QLabel)




EXTENSION_ID = 'pykrita_brushes_fixer_special'
MENU_ENTRY = 'Brushes Fixer (for 5.1 beta 1 brushes)'



class BrushesFixerSpecial(Extension):

	files_list = []
	textbox = None
	dialog = None
	
	
	def __init__(self, parent):
		super().__init__(parent)

	def setup(self):
		pass

	def createActions(self, window):
		action = window.createAction(EXTENSION_ID, MENU_ENTRY, "tools/scripts")
		# parameter 1 = the name that Krita uses to identify the action
		# parameter 2 = the text to be added to the menu entry for this script
		# parameter 3 = location of menu entry
		action.triggered.connect(self.action_triggered)
	
	
	# returns correct metadata
	def reconvert_metadata(self, metadata):
		metadata = metadata.replace("&amp;", "&")
		metadata = metadata.replace("&apos;", "'")
		metadata = metadata.replace("&quot;", "\"")
		metadata = metadata.replace("&lt;", "<")
		metadata = metadata.replace("&gt;", ">")
		
		return metadata
		
	
	# returns success value (true or false)
	def restore_brush(self, filename, make_backup):
		# copy to _backup brush
		r = True
		if make_backup:
			fi = QFileInfo(filename)
			ff = QFile(filename)
			copy_filename = fi.path() + "/" + fi.baseName() + "_backup." + fi.completeSuffix()
			if QFileInfo(copy_filename).exists():
				QFile(copy_filename).remove()
			r = ff.copy(copy_filename)
			if not r:
				return r
		
		# change the metadata
		preset = QImage(filename)
		if preset:
			metadata = preset.text("preset")
			correct_metadata = self.reconvert_metadata(metadata)
			
			png_filename = fi.path() + "/" + fi.baseName() + ".png"
			
			preset.setText("preset", correct_metadata)
			r = preset.save(png_filename)
			if not r:
				return False
			kppff = QFile(filename)
			r = kppff.remove() # otherwise renaming won't work
			if not r:
				return False
			
			pngff = QFile(png_filename)
			r = pngff.rename(filename)
		else:
			r = False
		
		return r
		
	def buttonClicked(self):
		fd = QFileDialog()
		fd.setFileMode(QFileDialog.ExistingFiles)
		fd.exec()
		if fd.result() == QDialog.Accepted:
			self.files_list = fd.selectedFiles()
			self.textbox.setText("\n".join(self.files_list))
		
	def cancelButtonClicked(self):
		self.dialog.reject()
	
	def okButtonClicked(self):
		self.dialog.accept()
	
	
	def create_dialog(self):
		dialog = QDialog()
		
		button = QPushButton("Select files", dialog)
		textbox = QTextEdit()
		self.textbox = textbox
		layout = QVBoxLayout(dialog)
		dialog.setLayout(layout)
		
		
		warning1 = QLabel("!!! It's best to back up your resource folder or bundle before you start editing it, it's just a few hours of work, just tested by myself.")
		warning2 = QLabel("!!! This will create backup files, invisible to Krita when *in a folder*.")
		warning3 = QLabel("!!! If you have a bundle, you need to unzip it, convert brush presets, remove backup files and zip up again.")
		
		okButton = QPushButton("OK", dialog)
		cancelButton = QPushButton("Cancel", dialog)
		
		layout.addWidget(warning1)
		layout.addWidget(warning2)
		layout.addWidget(warning3)
		
		
		layout.addWidget(textbox)
		layout.addWidget(button)
		layout.addWidget(okButton)
		layout.addWidget(cancelButton)
		
		
		okButton.setDefault(True)
		self.dialog = dialog
		
		
		
		button.clicked.connect(self.buttonClicked)
		cancelButton.clicked.connect(self.cancelButtonClicked)
		okButton.clicked.connect(self.okButtonClicked)
		
		
		
		
		return dialog
		
		
		
	
	def action_triggered(self):
		d = self.create_dialog()
		d.exec()
		if d.result() == QDialog.Accepted:
			
			failed_files = []
			for brush in self.files_list:
				r = self.restore_brush(brush, True)
				if not r:
					failed_files.append(brush)
			
			if len(failed_files) > 0:
				m = QMessageBox()
				m.setText("Some files cannot be reconverted (" + str(len(failed_files)) + " out of " + str(len(self.files_list)) + " submitted):")
				m.setInformativeText("\n".join(failed_files))
				m.setIcon(QMessageBox.Warning)
				
				m.exec()
			
			m = QMessageBox()
			m.setText("IMPORTANT: Restart Krita!                          ")
			m.setInformativeText("If you changed presets inside Krita's resource folder, you need to save all of your open files and restart Krita.")
			m.setIcon(QMessageBox.Warning)
			m.exec()
		
		






