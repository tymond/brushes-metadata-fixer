# Installation

- Use the `Tools -> Scripts -> Import Python Plugin from Web...` and use the link to the zip file (https://invent.kde.org/tymond/brushes-metadata-fixer/-/archive/master/brushes-metadata-fixer-master.zip). 
- Or download zip separately and use `Tools -> Scripts -> Import Python Plugin from file...`. 
- Or just unzip, enter the `pykrita` directory, and copy both the `.desktop` file and the folder to your `pykrita` folder in your resource folder.

Then restart Krita.

# Usage

1. Prepare the files:
    - if you just have some .kpp files, you're good to go
    - if you have a bundle you want to fix, you need to change the extension to `.zip` and unzip it somewhere.
2. Make sure you have backups of the files you want to convert, preferebly backups of the whole folder or bundle.
3. Go to `Tools -> Scripts -> Brush Fixer (for 5.1 beta 1 brushes)`.
4. Press `Select files`, find the files and select them. In the textbox above you can see the selected files.
5. Press OK.
6. If you converted files from a bundle, you need to:
    - remove the backup files (I assume you have a backup of the bundle somewhere - you should!)
    - zip the folder again and change the extension back to `.bundle`.
7. Restart Krita.

# Feedback

In this thread: https://krita-artists.org/t/brush-fixer-for-brushes-created-in-krita-5-1-beta-1/44075
Merge requests welcome on KDE Invent.
